/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <QtCore>

#include <AST>
#include <CodeGen>

#include "classsmoker.h"
#include "smokeloader.h"

int main(int argc, char** argv)
{
    SmokeLoader loader("qtcore");

    SmokePtr smoke = loader.smoke();

    AST::Unit unit;
    AST::Package *package = new AST::Package("org.kde.qtcore");
    unit.append(package);

    ClassSmoker classGen(package, smoke.data());

    classGen.run();

    CodeGen::UnitGenerator unitGen(&unit);
    unitGen.generate();
}
