/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef AST_METHOD_H
#define AST_METHOD_H

#include <QList>

#include "parameter.h"
#include "typedmember.h"
#include "statement.h"

namespace AST {

class MethodPrivate : public TypedMemberPrivate
{
public:
    MethodPrivate(const QString& name, const TypeReference& type, Member::Access access);
    virtual ~MethodPrivate();

    QList<Parameter*> parameters;
    QList<Statement*> statements;
};

class Method : public TypedMember
{
public:

    Method(const QString& name, const TypeReference& type = "void", Member::Access access = Member::Public);
    virtual ~Method();

    QList<Parameter*>& parameters();
    const QList<Parameter*>& parameters() const;

    QList<Statement*>& statements();
    const QList<Statement*>& statements() const;

    void accept(Visitor *visitor) const;

protected:
    Method(MethodPrivate* dd);

private:
    Q_DECLARE_PRIVATE(Method)
};

}

#endif // AST_METHOD_H
