/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef AST_NEWEXPRESSION_H
#define AST_NEWEXPRESSION_H

#include <QList>

#include <ast/expression.h>
#include "typereference.h"

namespace AST {

class NewExpressionPrivate : public ExpressionPrivate
{
public:
    NewExpressionPrivate(const TypeReference& type) : type(type) {}
    virtual ~NewExpressionPrivate() {
        qDeleteAll(parameters);
    }

    TypeReference type;
    QList<Expression*> parameters;
};

class NewExpression : public AST::Expression
{
public:

    NewExpression(const TypeReference& type);
    virtual ~NewExpression();

    TypeReference type() const;
    void setType(const TypeReference& type);

    QList<Expression*>& parameters();
    const QList<Expression*>& parameters() const;

    virtual void accept(AST::Visitor* visitor) const;

protected:
    NewExpression(NewExpressionPrivate *dd);

private:
    Q_DECLARE_PRIVATE(NewExpression)
};

}

#endif
