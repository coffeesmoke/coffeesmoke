/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "statement.h"

using namespace AST;

Statement::Statement()
    : d_ptr(new StatementPrivate)
{
}

Statement::Statement(const Statement& other)
    : d_ptr(other.d_ptr)
{
}

Statement::Statement(StatementPrivate* dd)
    : d_ptr(dd)
{
}

Statement::~Statement()
{
    delete d_ptr;
}

void AST::Statement::accept(Visitor* visitor) const
{
    visitor->visit(this);
}
