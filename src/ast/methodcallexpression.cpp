/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "methodcallexpression.h"

#include "method.h"

using namespace AST;

MethodCallExpression::MethodCallExpression(Expression* target, const QString& methodName, QList< Expression* > params)
    : Expression(new MethodCallExpressionPrivate(target, methodName, params))
{
}

MethodCallExpression::MethodCallExpression(Expression* target, const AST::Method& method, QList< Expression* > params)
    : Expression(new MethodCallExpressionPrivate(target, method.name(), params))
{
}

MethodCallExpression::MethodCallExpression(MethodCallExpressionPrivate *dd)
    : Expression(dd)
{
}

Expression* MethodCallExpression::target() const
{
    Q_D(const MethodCallExpression);
    return d->target;
}

void MethodCallExpression::setTarget(Expression* target)
{
    Q_D(MethodCallExpression);
    d->target = target;
}

QString MethodCallExpression::methodName() const
{
    Q_D(const MethodCallExpression);
    return d->methodName;
}

void MethodCallExpression::setMethodName(const QString& name)
{
    Q_D(MethodCallExpression);
    d->methodName = name;
}

const QList< Expression* >& MethodCallExpression::parameters() const
{
    Q_D(const MethodCallExpression);
    return d->params;
}

QList< Expression* >& MethodCallExpression::parameters()
{
    Q_D(MethodCallExpression);
    return d->params;
}

void AST::MethodCallExpression::accept(Visitor* visitor) const
{
    visitor->visit(this);
}
