/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "variabledeclarationstatement.h"

using namespace AST;

VariableDeclarationStatement::VariableDeclarationStatement(const AST::TypeReference& type, const QString& name)
    : Declaration(new VariableDeclarationStatementPrivate(type, name))
{
}

VariableDeclarationStatement::VariableDeclarationStatement(VariableDeclarationStatementPrivate* dd)
    : Declaration(dd)
{
}

VariableDeclarationStatement::~VariableDeclarationStatement()
{
}

TypeReference VariableDeclarationStatement::type() const
{
    Q_D(const VariableDeclarationStatement);
    return d->type;
}

void VariableDeclarationStatement::setType(const AST::TypeReference& type)
{
    Q_D(VariableDeclarationStatement);
    d->type = type;
}

Expression* VariableDeclarationStatement::initExpression() const
{
    Q_D(const VariableDeclarationStatement);
    return d->init;
}

void VariableDeclarationStatement::setInitExpression(Expression* expr)
{
    Q_D(VariableDeclarationStatement);
    d->init = expr;
}

void AST::VariableDeclarationStatement::accept(Visitor* visitor) const
{
    visitor->visit(this);
}
