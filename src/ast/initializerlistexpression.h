/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef AST_INITIALIZERLISTEXPRESSION_H
#define AST_INITIALIZERLISTEXPRESSION_H

#include <QList>

#include "expression.h"

namespace AST {

class InitializerListExpressionPrivate : public ExpressionPrivate
{
public:
    InitializerListExpressionPrivate() {}
    virtual ~InitializerListExpressionPrivate() {
        qDeleteAll(expressions);
    }

    QList<Expression*> expressions;
};

class InitializerListExpression : public AST::Expression
{
public:
    InitializerListExpression();
    virtual ~InitializerListExpression();

    QList<Expression*>& expressions();
    const QList<Expression*>& expressions() const;

    void accept(AST::Visitor* visitor);

protected:
    InitializerListExpression(InitializerListExpressionPrivate *dd);

private:
    Q_DECLARE_PRIVATE(InitializerListExpression)
};

}

#endif // AST_INITIALIZERLISTEXPRESSION_H
