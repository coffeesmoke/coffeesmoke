/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "field.h"

using namespace AST;

Field::Field(const AST::TypeReference& type, const QString& name, Member::Access access)
    : TypedMember(new FieldPrivate(type, name, access))
{
}

Field::Field(TypedMemberPrivate* dd)
    : TypedMember(dd)
{
}

Field::~Field()
{
}

Expression* Field::initExpression() const
{
    Q_D(const Field);
    return d->init;
}

void Field::setInitExpression(Expression* expression)
{
    Q_D(Field);
    d->init = expression;
}

void Field::accept(AST::Visitor* visitor) const
{
    visitor->visit(this);
}

