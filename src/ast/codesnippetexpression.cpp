/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "codesnippetexpression.h"

using namespace AST;

CodeSnippetExpression::CodeSnippetExpression(const QString& snippet)
    : Expression(new CodeSnippetExpressionPrivate(snippet))
{
}

CodeSnippetExpression::CodeSnippetExpression(CodeSnippetExpressionPrivate* dd)
    : Expression(dd)
{
}

CodeSnippetExpression::~CodeSnippetExpression()
{
}

QString CodeSnippetExpression::snippet() const
{
    Q_D(const CodeSnippetExpression);
    return d->snippet;
}

void CodeSnippetExpression::setSnippet(const QString& snippet)
{
    Q_D(CodeSnippetExpression);
    d->snippet = snippet;
}

void CodeSnippetExpression::accept(AST::Visitor* visitor) const
{
    visitor->visit(this);
}
