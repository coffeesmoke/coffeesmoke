/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef AST_UNIT_H
#define AST_UNIT_H

#include <QString>

#include "collection.h"
#include "declaration.h"
#include "visitable.h"

namespace AST {

class Declaration;

class UnitPrivate
{
public:
    UnitPrivate(const QString& name = QString());
    virtual ~UnitPrivate();

    QString name;
};

class Unit : public Collection<Declaration*>, public Visitable
{
public:
    Unit(const QString& name = QString());
    virtual ~Unit();

    QString name() const;
    void setName(const QString& name);

    void accept(Visitor *visitor) const;

protected:
    Unit(UnitPrivate *dd);
    UnitPrivate *const d_ptr;

private:
    Q_DECLARE_PRIVATE(Unit)
    Q_DISABLE_COPY(Unit)
};

}

#endif // AST_UNIT_H
