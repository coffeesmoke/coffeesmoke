/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef AST_COLLECTION_H
#define AST_COLLECTION_H

#include <QList>

namespace AST
{

/**
 * This is actually a QList, but it has a virtual destructor.
 */
template<typename T>
class Collection : QList<T>
{
public:
    Collection() {}
    Collection(const QList<T>& list) : QList<T>(list) {}
    Collection(const Collection<T>& other) : QList<T>(other) {}
    virtual ~Collection() {}

    using QList<T>::const_iterator;
    using QList<T>::iterator;
    using QList<T>::ConstIterator;
    using QList<T>::Iterator;

    using QList<T>::append;
    using QList<T>::at;
    using QList<T>::back;
    using QList<T>::begin;
    using QList<T>::clear;
    using QList<T>::constBegin;
    using QList<T>::constEnd;
    using QList<T>::contains;
    using QList<T>::count;
    using QList<T>::empty;
    using QList<T>::end;
    using QList<T>::endsWith;
    using QList<T>::erase;
    using QList<T>::first;
    using QList<T>::front;
    using QList<T>::indexOf;
    using QList<T>::insert;
    using QList<T>::isEmpty;
    using QList<T>::last;
    using QList<T>::lastIndexOf;
    using QList<T>::length;
    using QList<T>::mid;
    using QList<T>::move;
    using QList<T>::pop_back;
    using QList<T>::pop_front;
    using QList<T>::prepend;
    using QList<T>::push_back;
    using QList<T>::push_front;
    using QList<T>::removeAll;
    using QList<T>::removeAt;
    using QList<T>::removeFirst;
    using QList<T>::removeLast;
    using QList<T>::removeOne;
    using QList<T>::replace;
    using QList<T>::size;
    using QList<T>::startsWith;
    using QList<T>::swap;
    using QList<T>::takeAt;
    using QList<T>::takeFirst;
    using QList<T>::takeLast;
    using QList<T>::toSet;
    using QList<T>::toStdList;
    using QList<T>::toVector;
    using QList<T>::value;
    using QList<T>::operator!=;
    using QList<T>::operator+;
    using QList<T>::operator+=;
    using QList<T>::operator<<;
    using QList<T>::operator=;
    using QList<T>::operator==;
    using QList<T>::operator[];
};

}

#endif // AST_COLLECTION_H
