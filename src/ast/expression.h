/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef AST_EXPRESSION_H
#define AST_EXPRESSION_H

#include <QSharedData>
#include <QSharedPointer>
#include <QString>

#include "visitable.h"

namespace AST {

class ExpressionPrivate
{
public:
    ExpressionPrivate() : valid(true) {}
    ExpressionPrivate(bool isValid) : valid(isValid) {}
    virtual ~ExpressionPrivate() {}

    bool valid;
};

class Expression : public Visitable
{
public:

    Expression();
    virtual ~Expression();

    void accept(Visitor *visitor) const;

    bool isValid() const;

protected:
    Expression(ExpressionPrivate *dd);
    ExpressionPrivate * const d_ptr;

private:
    Q_DECLARE_PRIVATE(Expression)
    Q_DISABLE_COPY(Expression);
};

}

#endif // AST_EXPRESSION_H
