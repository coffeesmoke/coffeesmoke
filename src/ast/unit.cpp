/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "unit.h"

using namespace AST;

UnitPrivate::UnitPrivate(const QString& name)
    : name(name)
{
}

UnitPrivate::~UnitPrivate()
{
}


Unit::Unit(const QString& name)
    : d_ptr(new UnitPrivate(name))
{
}

Unit::Unit(UnitPrivate* dd)
    : d_ptr(dd)
{
}

Unit::~Unit()
{
    qDeleteAll(begin(), end());

    delete d_ptr;
}

QString Unit::name() const
{
    Q_D(const Unit);
    return d->name;
}

void Unit::setName(const QString& name)
{
    Q_D(Unit);
    d->name = name;
}

void AST::Unit::accept(Visitor* visitor) const
{
    visitor->visit(this);
}
