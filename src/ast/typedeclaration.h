/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef AST_TYPEDECLARATION_H
#define AST_TYPEDECLARATION_H

#include <QList>
#include <QStringList>

#include "importdeclaration.h"
#include "member.h"

namespace AST {

class TypeDeclarationPrivate : public MemberPrivate
{
public:
    TypeDeclarationPrivate(const QString& name, Member::Access access) : MemberPrivate(name, access), interface(false) {}
    virtual ~TypeDeclarationPrivate() {
        qDeleteAll(members);
        qDeleteAll(imports);
    }

    QList<Member*> members;
    QList<ImportDeclaration*> imports;
    bool interface;
    QStringList bases;
    QStringList interfaces;
};

class TypeDeclaration : public Member
{
public:

    TypeDeclaration(const QString& name, Member::Access access = Member::Public);
    virtual ~TypeDeclaration();

    QList<Member*>& members();
    const QList<Member*>& members() const;

    QList<ImportDeclaration*>& imports();
    const QList<ImportDeclaration*>& imports() const;

    bool isInterface() const;
    void setIsInterface(bool isInterface);

    QStringList& baseTypes();
    const QStringList& baseTypes() const;

    QStringList& interfaces();
    const QStringList& interfaces() const;

    void accept(Visitor *visitor) const;

protected:
    TypeDeclaration(TypeDeclarationPrivate* dd);

private:
    Q_DECLARE_PRIVATE(TypeDeclaration)
};

}

#endif // AST_TYPEDECLARATION_H
