/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "expressionstatement.h"

using namespace AST;

ExpressionStatement::ExpressionStatement(Expression* expression)
    : Statement(new ExpressionStatementPrivate(expression))
{
}

ExpressionStatement::ExpressionStatement(ExpressionStatementPrivate* dd)
    : Statement(dd)
{
}

ExpressionStatement::~ExpressionStatement()
{
}

Expression* ExpressionStatement::expression() const
{
    Q_D(const ExpressionStatement);
    return d->expression;
}

void ExpressionStatement::setExpression(Expression* expression)
{
    Q_D(ExpressionStatement);
    d->expression = expression;
}

void AST::ExpressionStatement::accept(Visitor* visitor) const
{
    visitor->visit(this);
}
