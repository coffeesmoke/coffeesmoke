/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "assignmentexpression.h"

using namespace AST;

AssignmentExpression::AssignmentExpression(Expression* lhs, Expression* rhs)
    : Expression(new AssignmentExpressionPrivate(lhs, rhs))
{
}

AssignmentExpression::AssignmentExpression(AssignmentExpressionPrivate* dd)
    : Expression(dd)
{
}

AssignmentExpression::~AssignmentExpression()
{
}

Expression* AssignmentExpression::lhs() const
{
    Q_D(const AssignmentExpression);
    return d->lhs;
}

void AssignmentExpression::setLhs(Expression* lhs)
{
    Q_D(AssignmentExpression);
    d->lhs = lhs;
}

Expression* AssignmentExpression::rhs() const
{
    Q_D(const AssignmentExpression);
    return d->rhs;
}

void AssignmentExpression::setRhs(Expression* rhs)
{
    Q_D(AssignmentExpression);
    d->rhs = rhs;
}

void AssignmentExpression::accept(AST::Visitor* visitor) const
{
    visitor->visit(this);
}

