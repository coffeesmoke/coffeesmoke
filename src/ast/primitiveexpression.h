/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef AST_PRIMITIVEEXPRESSION_H
#define AST_PRIMITIVEEXPRESSION_H

#include <QVariant>

#include "expression.h"

namespace AST {

class PrimitiveExpressionPrivate : public ExpressionPrivate
{
public:
    PrimitiveExpressionPrivate(const QVariant& value) : value(value) {}
    virtual ~PrimitiveExpressionPrivate() {}

    QVariant value;
};

class PrimitiveExpression : public AST::Expression
{
public:

    PrimitiveExpression(const QVariant& value);
    virtual ~PrimitiveExpression();

    QVariant value() const;
    void setValue(const QVariant& value);

    void accept(AST::Visitor* visitor) const;

protected:
    PrimitiveExpression(PrimitiveExpressionPrivate *dd);

private:
    Q_DECLARE_PRIVATE(PrimitiveExpression)
};

}

#endif // AST_PRIMITIVEEXPRESSION_H
