/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "assignmentexpression.h"
#include "codesnippetexpression.h"
#include "declaration.h"
#include "expression.h"
#include "expressionstatement.h"
#include "field.h"
#include "member.h"
#include "method.h"
#include "methodcallexpression.h"
#include "newexpression.h"
#include "importdeclaration.h"
#include "initializerlistexpression.h"
#include "package.h"
#include "parameter.h"
#include "primitiveexpression.h"
#include "referenceexpression.h"
#include "statement.h"
#include "typedeclaration.h"
#include "typedmember.h"
#include "unit.h"
#include "variabledeclarationstatement.h"

#include "visitable.h"

#include "visitor.h"

using namespace AST;

void Visitor::visit(const AST::Visitable* visitable)
{
    // default: do nothing.
}

void Visitor::visit(const AST::AssignmentExpression* assign)
{
    visit(static_cast<const Expression*>(assign));
}

void Visitor::visit(const AST::CodeSnippetExpression* snippet)
{
    visit(static_cast<const Expression*>(snippet));
}

void Visitor::visit(const AST::Declaration* declaration)
{
    visit(static_cast<const Statement*>(declaration));
}

void Visitor::visit(const AST::Expression* expression)
{
    visit(static_cast<const Visitable*>(expression));
}

void Visitor::visit(const AST::ExpressionStatement* expressionStatement)
{
    visit(static_cast<const Statement*>(expressionStatement));
}

void Visitor::visit(const AST::Field* field)
{
    visit(static_cast<const TypedMember*>(field));
}

void Visitor::visit(const AST::Member* member)
{
    visit(static_cast<const Declaration*>(member));
}

void Visitor::visit(const AST::Method* method)
{
    visit(static_cast<const TypedMember*>(method));
}

void Visitor::visit(const AST::MethodCallExpression* methodCallExpression)
{
    visit(static_cast<const Expression*>(methodCallExpression));
}

void Visitor::visit(const AST::NewExpression* newExpression)
{
    visit(static_cast<const Expression*>(newExpression));
}

void Visitor::visit(const AST::ImportDeclaration* import)
{
    visit(static_cast<const Declaration*>(import));
}

void Visitor::visit(const AST::InitializerListExpression* initList)
{
    visit(static_cast<const Expression*>(initList));
}

void Visitor::visit(const AST::Package* package)
{
    visit(static_cast<const Declaration*>(package));
}

void Visitor::visit(const AST::Parameter* parameter)
{
    visit(static_cast<const Declaration*>(parameter));
}

void Visitor::visit(const AST::PrimitiveExpression* primitive)
{
    visit(static_cast<const Expression*>(primitive));
}

void Visitor::visit(const AST::Statement* statement)
{
    visit(static_cast<const Visitable*>(statement));
}

void Visitor::visit(const AST::ReferenceExpression* reference)
{
    visit(static_cast<const Expression*>(reference));
}

void Visitor::visit(const AST::TypeDeclaration* typeDeclaration)
{
    visit(static_cast<const Member*>(typeDeclaration));
}

void Visitor::visit(const AST::TypedMember* typedMember)
{
    visit(static_cast<const Member*>(typedMember));
}

void Visitor::visit(const AST::VariableDeclarationStatement* variableDeclarationExpression)
{
    visit(static_cast<const Statement*>(variableDeclarationExpression));
}

void Visitor::visit(const AST::Unit* unit)
{
    visit(static_cast<const Visitable*>(unit));
}
