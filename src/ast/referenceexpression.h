/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef AST_REFERENCEEXPRESSION_H
#define AST_REFERENCEEXPRESSION_H

#include <ast/expression.h>

namespace AST {

class ReferenceExpressionPrivate : public ExpressionPrivate
{
public:
    ReferenceExpressionPrivate(const QString& target) : target(target) {}

    QString target;
};

class ReferenceExpression : public AST::Expression
{
public:

    ReferenceExpression(const QString& target);
    virtual ~ReferenceExpression();

    QString target() const;
    void setTarget(const QString& target);

    virtual void accept(AST::Visitor* visitor) const;

protected:
    ReferenceExpression(ReferenceExpressionPrivate *dd);

private:
    Q_DECLARE_PRIVATE(ReferenceExpression)
};

}

#endif // AST_REFERENCEEXPRESSION_H
