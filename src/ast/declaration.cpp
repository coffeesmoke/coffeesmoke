/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "declaration.h"

using namespace AST;

Declaration::Declaration(const QString& name)
    : Statement(new DeclarationPrivate(name))
{
}

Declaration::Declaration(DeclarationPrivate* dd)
    : Statement(dd)
{
}

Declaration::~Declaration()
{
}

QString Declaration::name() const
{
    Q_D(const Declaration);
    return d->name;
}

void Declaration::setName(const QString& name)
{
    Q_D(Declaration);
    d->name = name;
}

Declaration::Modifiers Declaration::modifiers() const
{
    Q_D(const Declaration);
    return d->modifiers;
}

void Declaration::setModifiers(Declaration::Modifiers modifiers)
{
    Q_D(Declaration);
    d->modifiers = modifiers;
}

void Declaration::addModifier(Declaration::Modifier modifier)
{
    Q_D(Declaration);
    d->modifiers |= modifier;
}

void Declaration::removeModifier(Declaration::Modifier modifier)
{
    Q_D(Declaration);
    d->modifiers &= ~modifier;
}

void AST::Declaration::accept(Visitor* visitor) const
{
    visitor->visit(this);
}
