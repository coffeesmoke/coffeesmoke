/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef AST_PACKAGE_H
#define AST_PACKAGE_H

#include "collection.h"
#include "declaration.h"

namespace AST
{

class PackagePrivate : public DeclarationPrivate
{
public:
    PackagePrivate(const QString& name);
    virtual ~PackagePrivate();
};

class Package : public Collection<Declaration*>, public Declaration
{
public:

    Package(const QString& name);
    virtual ~Package();

    void accept(Visitor *visitor) const;

protected:
    Package(PackagePrivate* dd);

private:
    Q_DECLARE_PRIVATE(Package)
};

}

#endif // AST_PACKAGE_H
