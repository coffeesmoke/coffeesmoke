/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "typedeclaration.h"

#include "member.h"

using namespace AST;

TypeDeclaration::TypeDeclaration(const QString& name, Member::Access access)
    : Member(new TypeDeclarationPrivate(name, access))
{
    addModifier(Member::Static);
}

TypeDeclaration::TypeDeclaration(TypeDeclarationPrivate* dd)
    : Member(dd)
{
}

TypeDeclaration::~TypeDeclaration()
{
}

QList< AST::Member* >& AST::TypeDeclaration::members()
{
    Q_D(TypeDeclaration);
    return d->members;
}

const QList< AST::Member* >& AST::TypeDeclaration::members() const
{
    Q_D(const TypeDeclaration);
    return d->members;
}

QList< ImportDeclaration* >& TypeDeclaration::imports()
{
    Q_D(TypeDeclaration);
    return d->imports;
}

const QList< ImportDeclaration* >& TypeDeclaration::imports() const
{
    Q_D(const TypeDeclaration);
    return d->imports;
}

bool TypeDeclaration::isInterface() const
{
    Q_D(const TypeDeclaration);
    return d->interface;
}

void TypeDeclaration::setIsInterface(bool isInterface)
{
    Q_D(TypeDeclaration);
    d->interface = isInterface;
}

const QStringList& TypeDeclaration::baseTypes() const
{
    Q_D(const TypeDeclaration);
    return d->bases;
}

QStringList& TypeDeclaration::baseTypes()
{
    Q_D(TypeDeclaration);
    return d->bases;
}

QStringList& TypeDeclaration::interfaces()
{
    Q_D(TypeDeclaration);
    return d->interfaces;
}

const QStringList& TypeDeclaration::interfaces() const
{
    Q_D(const TypeDeclaration);
    return d->interfaces;
}

void AST::TypeDeclaration::accept(Visitor* visitor) const
{
    visitor->visit(this);
}
