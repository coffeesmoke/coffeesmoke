/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef AST_EXPRESSIONSTATEMENT_H
#define AST_EXPRESSIONSTATEMENT_H

#include "expression.h"
#include "statement.h"

namespace AST {

class ExpressionStatementPrivate : public AST::StatementPrivate
{
public:
    ExpressionStatementPrivate(Expression* expr) : expression(expr) {}
    virtual ~ExpressionStatementPrivate() {
        delete expression;
    }

    Expression* expression;
};

class ExpressionStatement : public AST::Statement
{
public:

    ExpressionStatement(Expression* expression);
    virtual ~ExpressionStatement();

    Expression* expression() const;
    void setExpression(Expression* expression);

    void accept(Visitor *visitor) const;

protected:
    ExpressionStatement(ExpressionStatementPrivate *dd);

private:
    Q_DECLARE_PRIVATE(ExpressionStatement)
};

}

#endif // AST_EXPRESSIONSTATEMENT_H
