/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "referenceexpression.h"

using namespace AST;

ReferenceExpression::ReferenceExpression(const QString& target)
    : Expression(new ReferenceExpressionPrivate(target))
{
}

ReferenceExpression::~ReferenceExpression()
{
}

ReferenceExpression::ReferenceExpression(ReferenceExpressionPrivate* dd)
    : Expression(dd)
{
}

void ReferenceExpression::setTarget(const QString& target)
{
    Q_D(ReferenceExpression);
    d->target = target;
}

QString ReferenceExpression::target() const
{
    Q_D(const ReferenceExpression);
    return d->target;
}

void ReferenceExpression::accept(AST::Visitor* visitor) const
{
    visitor->visit(this);
}

