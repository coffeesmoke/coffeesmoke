/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef AST_DECLARATION_H
#define AST_DECLARATION_H

#include "statement.h"

namespace AST {

class DeclarationPrivate;

class Declaration : public AST::Statement
{
public:

    Declaration(const QString& name);
    virtual ~Declaration();

    QString name() const;
    void setName(const QString& name);

    enum Modifier {
        None = 0,
        Abstract = 0x01,
        Final = 0x02,
        Static = 0x04,
    };
    Q_DECLARE_FLAGS(Modifiers, Modifier)

    Modifiers modifiers() const;
    void setModifiers(Modifiers modifiers);
    void addModifier(Modifier modifier);
    void removeModifier(Modifier modifier);

    void accept(Visitor *visitor) const;

protected:
    Declaration(DeclarationPrivate *dd);

private:
    Q_DECLARE_PRIVATE(Declaration)
};

class DeclarationPrivate : public StatementPrivate
{
public:
    DeclarationPrivate(const QString& name) : name(name), modifiers(Declaration::None) {}
    virtual ~DeclarationPrivate() {}

    QString name;
    Declaration::Modifiers modifiers;
};

}

#endif // AST_DECLARATION_H
