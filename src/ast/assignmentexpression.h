/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef AST_ASSIGNMENTEXPRESSION_H
#define AST_ASSIGNMENTEXPRESSION_H

#include <ast/expression.h>

namespace AST {

class AssignmentExpressionPrivate : public ExpressionPrivate
{
public:
    AssignmentExpressionPrivate(Expression* lhs, Expression* rhs) : lhs(lhs), rhs(rhs) {}
    virtual ~AssignmentExpressionPrivate() {
        delete lhs;
        delete rhs;
    }

    Expression* lhs;
    Expression* rhs;
};

class AssignmentExpression : public AST::Expression
{
public:

    AssignmentExpression(Expression* lhs, Expression* rhs);
    virtual ~AssignmentExpression();

    Expression* lhs() const;
    void setLhs(Expression* lhs);

    Expression* rhs() const;
    void setRhs(Expression* rhs);

    virtual void accept(AST::Visitor* visitor) const;

protected:
    AssignmentExpression(AssignmentExpressionPrivate *dd);

private:
    Q_DECLARE_PRIVATE(AssignmentExpression)
};

}

#endif // AST_ASSIGNMENTEXPRESSION_H
