/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef AST_VISITOR_H
#define AST_VISITOR_H

namespace AST {

class Visitable;

class AssignmentExpression;
class CodeSnippetExpression;
class Declaration;
class Expression;
class ExpressionStatement;
class Field;
class ImportDeclaration;
class InitializerListExpression;
class Member;
class Method;
class MethodCallExpression;
class NewExpression;
class Package;
class Parameter;
class PrimitiveExpression;
class ReferenceExpression;
class Statement;
class TypeDeclaration;
class TypedMember;
class Unit;
class VariableDeclarationStatement;

/**
 * Used to process the AST hierarchy. By default, every \c visit method calls the \c visit method of the according base class.
 * So if an AST::Method is visited, the call will go as follows:
 *
 * \code
 * visit(AST::Method*); -> visit(AST::TypedMember*); -> visit(AST::Member*); -> visit(AST::Declaration*);
 * -> visit(AST::Statement*); -> visit(AST::Visitable*);
 * \endcode
 *
 * This allows you to e.g. only implement the \c AST::Member \c visit method when you're visiting \c AST::Methods or \c AST::Fields.
 *
 * It's your job to properly implement the \c visit methods that you care about in your specific Visitor.
 * This visitor does not call \c visit() for the children of the AST members; you have to do that yourself, too.
 */
class Visitor
{
public:
    virtual void visit(const Visitable *visitable);
    virtual void visit(const AssignmentExpression *assign);
    virtual void visit(const CodeSnippetExpression *snippet);
    virtual void visit(const Declaration *declaration);
    virtual void visit(const Expression *expression);
    virtual void visit(const ExpressionStatement *expressionStatement);
    virtual void visit(const Field *field);
    virtual void visit(const Member *member);
    virtual void visit(const Method *method);
    virtual void visit(const MethodCallExpression *methodCallExpression);
    virtual void visit(const NewExpression *newExpression);
    virtual void visit(const ImportDeclaration *import);
    virtual void visit(const InitializerListExpression *initList);
    virtual void visit(const Package *package);
    virtual void visit(const Parameter *parameter);
    virtual void visit(const PrimitiveExpression *primitive);
    virtual void visit(const ReferenceExpression *reference);
    virtual void visit(const Statement *statement);
    virtual void visit(const TypeDeclaration *typeDeclaration);
    virtual void visit(const TypedMember *typedMember);
    virtual void visit(const Unit *unit);
    virtual void visit(const VariableDeclarationStatement *variableDeclarationExpression);
};

}

#endif // AST_VISITOR_H
