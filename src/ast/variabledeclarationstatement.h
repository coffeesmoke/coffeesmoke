/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef AST_VARIABLEDECLARATIONSTATEMENT_H
#define AST_VARIABLEDECLARATIONSTATEMENT_H

#include "expression.h"
#include "declaration.h"
#include "typereference.h"

namespace AST {

class VariableDeclarationStatementPrivate : public AST::DeclarationPrivate
{
public:
    VariableDeclarationStatementPrivate(const TypeReference& type, const QString& name) : DeclarationPrivate(name), type(type) {}
    virtual ~VariableDeclarationStatementPrivate() {
        delete init;
    }

    TypeReference type;
    Expression* init;
};

class VariableDeclarationStatement : public AST::Declaration
{
public:

    VariableDeclarationStatement(const TypeReference& type, const QString& name);
    virtual ~VariableDeclarationStatement();

    TypeReference type() const;
    void setType(const TypeReference& type);

    Expression* initExpression() const;
    void setInitExpression(Expression* expr);

    void accept(Visitor *visitor) const;

protected:
    VariableDeclarationStatement(VariableDeclarationStatementPrivate *dd);

private:
    Q_DECLARE_PRIVATE(VariableDeclarationStatement)
};

}

#endif // AST_VARIABLEDECLARATIONSTATEMENT_H
