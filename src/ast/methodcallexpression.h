/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef AST_METHODCALLEXPRESSION_H
#define AST_METHODCALLEXPRESSION_H

#include "expression.h"

#include <QList>

namespace AST {

class Method;

class MethodCallExpressionPrivate : public ExpressionPrivate
{
public:
    MethodCallExpressionPrivate(Expression* target, const QString& methodName, QList<Expression*> params = QList<Expression*>())
        : target(target), methodName(methodName), params(params) {}
    virtual ~MethodCallExpressionPrivate() {
        delete target;
        qDeleteAll(params);
    }

    Expression* target;
    QString methodName;
    QList<Expression*> params;
};

class MethodCallExpression : public AST::Expression
{
public:

    MethodCallExpression(Expression* target, const QString& methodName, QList<Expression*> params = QList<Expression*>());
    MethodCallExpression(Expression* target, const Method& method, QList<Expression*> params = QList<Expression*>());

    Expression* target() const;
    void setTarget(Expression* target);

    QString methodName() const;
    void setMethodName(const QString& name);

    const QList<Expression*>& parameters() const;
    QList<Expression*>& parameters();

    void accept(Visitor *visitor) const;

protected:
    MethodCallExpression(MethodCallExpressionPrivate *dd);

private:
    Q_DECLARE_PRIVATE(MethodCallExpression)
};

}

#endif // AST_METHODCALLEXPRESSION_H
