/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "newexpression.h"

using namespace AST;

NewExpression::NewExpression(const AST::TypeReference& type)
    : Expression(new NewExpressionPrivate(type))
{
}

NewExpression::NewExpression(NewExpressionPrivate* dd)
    : Expression(dd)
{
}

NewExpression::~NewExpression()
{
}

QList< Expression* >& NewExpression::parameters()
{
    Q_D(NewExpression);
    return d->parameters;
}

const QList< Expression* >& NewExpression::parameters() const
{
    Q_D(const NewExpression);
    return d->parameters;
}

void NewExpression::setType(const AST::TypeReference& type)
{
    Q_D(NewExpression);
    d->type = type;
}

TypeReference NewExpression::type() const
{
    Q_D(const NewExpression);
    return d->type;
}

void NewExpression::accept(AST::Visitor* visitor) const
{
    visitor->visit(this);
}

