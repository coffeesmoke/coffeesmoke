/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef AST_FIELD_H
#define AST_FIELD_H

#include <ast/expression.h>
#include <ast/typedmember.h>

namespace AST {

class FieldPrivate : public TypedMemberPrivate
{
public:
    FieldPrivate(const TypeReference& type, const QString& name, Member::Access access)
        : TypedMemberPrivate(name, access)
    {
        this->type = type;
    }
    virtual ~FieldPrivate() {
        delete init;
    }

    Expression* init;
};

class Field : public AST::TypedMember
{
public:

    Field(const TypeReference& type, const QString& name, Member::Access access = Member::Public);
    virtual ~Field();

    Expression* initExpression() const;
    void setInitExpression(Expression* expression);

    virtual void accept(AST::Visitor* visitor) const;

protected:
    Field(TypedMemberPrivate* dd);

private:
    Q_DECLARE_PRIVATE(Field)
};

}

#endif // AST_FIELD_H
