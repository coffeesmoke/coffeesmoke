/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef AST_TYPEDMEMBER_H
#define AST_TYPEDMEMBER_H

#include "member.h"
#include "typereference.h"

namespace AST {

class TypedMemberPrivate : public MemberPrivate
{
public:
    TypedMemberPrivate(const QString& name, Member::Access access);
    virtual ~TypedMemberPrivate();

    TypeReference type;
};

class TypedMember : public Member
{
public:

    virtual ~TypedMember();

    TypeReference type() const;
    void setType(const TypeReference& type);

    void accept(Visitor *visitor) const;

protected:
    TypedMember(TypedMemberPrivate* dd);

private:
    Q_DECLARE_PRIVATE(TypedMember)
};

}

#endif // AST_TYPEDMEMBER_H
