/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "method.h"

using namespace AST;

MethodPrivate::MethodPrivate(const QString& name, const AST::TypeReference& type, Member::Access access)
    : TypedMemberPrivate(name, access)
{
    this->type = type;
}

MethodPrivate::~MethodPrivate()
{
    qDeleteAll(parameters);
    qDeleteAll(statements);
}

Method::Method(const QString& name, const TypeReference& type, Member::Access access)
    : TypedMember(new MethodPrivate(name, type, access))
{
}

Method::Method(MethodPrivate* dd)
    : TypedMember(dd)
{
}

Method::~Method()
{
}

QList< Parameter* >& Method::parameters()
{
    Q_D(Method);
    return d->parameters;
}

const QList< Parameter* >& Method::parameters() const
{
    Q_D(const Method);
    return d->parameters;
}

QList< Statement* >& Method::statements()
{
    Q_D(Method);
    return d->statements;
}

const QList< Statement* >& Method::statements() const
{
    Q_D(const Method);
    return d->statements;
}

void AST::Method::accept(Visitor* visitor) const
{
    visitor->visit(this);
}
