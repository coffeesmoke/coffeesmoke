/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef ABSTRACTSMOKER_H
#define ABSTRACTSMOKER_H

#include <qglobal.h>

class Smoke;

class AbstractSmoker
{
public:
    AbstractSmoker(Smoke *smoke = 0);
    virtual ~AbstractSmoker();

    Smoke *smoke() const;
    void setSmoke(Smoke *smoke);

    virtual void run() = 0;

protected:
    Smoke *m_smoke;

private:
    Q_DISABLE_COPY(AbstractSmoker)
};

#endif // ABSTRACTSMOKER_H
