/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "smokeloader.h"

#include <QtCore>

SmokeLoaderData::SmokeLoaderData()
    : library(new QLibrary), smoke(0)
{
}

SmokeLoaderData::~SmokeLoaderData()
{
    delete library;
}


typedef void (*InitSmokeFn)();

SmokeLoader::SmokeLoader(const char *module)
    : d(new SmokeLoaderData)
{
    QRegExp exp("^(lib)?(smoke)?(.*)$");
    QString moduleName;

    QFileInfo fileInfo(module);
    if (fileInfo.exists()) {
        exp.exactMatch(fileInfo.baseName());
        moduleName = exp.cap(3);
    } else {
        exp.exactMatch(module);
        moduleName = exp.cap(3);
    }

    d->library = new QLibrary("smoke" + moduleName);
    d->moduleName = moduleName.toLatin1();
}

SmokeLoader::SmokeLoader(const QFileInfo& fileInfo)
{
    if (!fileInfo.exists()) {
        return;
    }

    QRegExp exp("^(lib)?(smoke)?(.*)$");

    exp.exactMatch(fileInfo.baseName());
    QString moduleName = exp.cap(3);

    d->library = new QLibrary("smoke" + moduleName);
    d->moduleName = moduleName.toLatin1();
}

SmokeLoader::~SmokeLoader()
{
}

bool SmokeLoader::load()
{
    if (d->smoke) {
        return true;
    }

    if (!d->library->load()) {
        return false;
    }

    QByteArray symbol = "init_" + d->moduleName + "_Smoke";
    InitSmokeFn init = (InitSmokeFn) d->library->resolve(symbol.constData());
    if (!init) {
        return false;
    }

    (*init)();

    symbol = d->moduleName + "_Smoke";
    d->smoke = SmokePtr(*static_cast<Smoke**>(d->library->resolve(symbol.constData())));

    if (!d->smoke) {
        return false;
    }

    return true;
}

bool SmokeLoader::isLoaded() const
{
    return d->smoke;
}

QString SmokeLoader::errorString() const
{
    return d->library->errorString();
}

SmokePtr SmokeLoader::smoke()
{
    load();
    return d->smoke;
}

SmokePtr SmokeLoader::smoke() const
{
    return d->smoke;
}
