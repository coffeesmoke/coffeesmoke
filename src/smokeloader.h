/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef SMOKELOADER_H
#define SMOKELOADER_H

#include <QSharedData>
#include <QSharedPointer>

#include <QByteArray>
#include <QLibrary>

#include <smoke.h>

class QFileInfo;

typedef QSharedPointer<Smoke> SmokePtr;

class SmokeLoaderData : public QSharedData
{
public:
    SmokeLoaderData();
    ~SmokeLoaderData();

    QLibrary *library;
    QByteArray moduleName;
    SmokePtr smoke;
};

class SmokeLoader
{
public:
    /**
     * Creates a SmokeLoader and tries to guess the correct SMOKE library name from \c module.
     */
    SmokeLoader(const char *module);
    /**
     * Creates a SmokeLoader which loads the SMOKE module from \c library.
     */
    SmokeLoader(const QFileInfo& library);

    ~SmokeLoader();

    /**
     * Returns whether the SMOKE module is loaded
     */
    bool isLoaded() const;
    /**
     * Explicitly tries to load the SMOKE module.
     */
    bool load();

    /**
     * Returns the error message that has occured during loading.
     */
    QString errorString() const;

    /**
     * Returns the SMOKE module. Loads it first, if necessary.
     */
    SmokePtr smoke();
    /**
     * Returns the SMOKE module. If none is loaded, returns 0.
     */
    SmokePtr smoke() const;

private:
    QExplicitlySharedDataPointer<SmokeLoaderData> d;
};

#endif // SMOKELOADER_H
