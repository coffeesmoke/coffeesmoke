/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <QTextStream>

#include <ast/expressionstatement.h>
#include <ast/variabledeclarationstatement.h>

#include "expressiongenerator.h"
#include "statementgenerator.h"

using namespace CodeGen;

StatementGenerator::StatementGenerator(const AST::Statement* statement)
    : m_statement(statement)
{
}

QString StatementGenerator::generate()
{
    m_string.clear();

    m_statement->accept(this);

    m_string.append(";\n");

    return m_string;
}

void StatementGenerator::visit(const AST::ExpressionStatement* expressionStatement)
{
    QTextStream out(&m_string);

    ExpressionGenerator expGen(expressionStatement->expression());
    QString string = expGen.generate();

    QTextStream lineReader(&string);

    while (!lineReader.atEnd()) {
        out << indentString() << lineReader.readLine();
    }
}

void StatementGenerator::visit(const AST::VariableDeclarationStatement* decl)
{
    QTextStream out(&m_string);

    out << indentString() << decl->type() << " " << decl->name();
    if (decl->initExpression()->isValid()) {
        out << " = ";
        ExpressionGenerator expGen(decl->initExpression());
        out << expGen.generate();
    }
}

