/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef CODEGEN_PACKAGEGENERATOR_H
#define CODEGEN_PACKAGEGENERATOR_H

#include <QDir>
#include <QTextStream>

#include <ast/importdeclaration.h>
#include <ast/package.h>
#include <ast/typedeclaration.h>

#include <codegen/abstractgenerator.h>

namespace CodeGen {

class PackageGenerator : public CodeGen::AbstractGenerator
{
public:
    PackageGenerator(const AST::Package* package, const QDir& outputDir = QDir::current());

    virtual QString generate();
    virtual void visit(const AST::ImportDeclaration* import);
    virtual void visit(const AST::Package* package);
    virtual void visit(const AST::TypeDeclaration* type);

protected:
    QTextStream out;
    QDir m_outputDir;

    const AST::Package *m_package;

    QStringList m_imports;

    bool m_gatherImports;
};

}

#endif // CODEGEN_PACKAGEGENERATOR_H
