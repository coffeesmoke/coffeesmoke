/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef CODEGEN_FIELDGENERATOR_H
#define CODEGEN_FIELDGENERATOR_H

#include <ast/field.h>
#include <codegen/abstractgenerator.h>

namespace CodeGen {

class FieldGenerator : public CodeGen::AbstractGenerator
{
public:
    FieldGenerator(const AST::Field *field);

    bool generateInitializer() const;
    void setGenerateInitializer(bool generate);

    virtual QString generate();
    virtual void visit(const AST::Field* field);

protected:
    QString m_string;

    const AST::Field *m_field;

    bool m_generateInitializer;
};

}

#endif // CODEGEN_FIELDGENERATOR_H
