/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef CODEGEN_STATEMENTGENERATOR_H
#define CODEGEN_STATEMENTGENERATOR_H

#include <ast/statement.h>
#include <codegen/abstractgenerator.h>

namespace CodeGen {

class StatementGenerator : public CodeGen::AbstractGenerator
{
public:
    StatementGenerator(const AST::Statement *statement);

    virtual QString generate();
    virtual void visit(const AST::ExpressionStatement* expressionStatement);
    virtual void visit(const AST::VariableDeclarationStatement* variableDeclarationExpression);

protected:
    const AST::Statement *m_statement;
    QString m_string;
};

}

#endif // CODEGEN_STATEMENTGENERATOR_H
