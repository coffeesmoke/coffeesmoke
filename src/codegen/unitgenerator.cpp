/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <ast/importdeclaration.h>
#include <ast/package.h>
#include <ast/typedeclaration.h>

#include "packagegenerator.h"
#include "typegenerator.h"
#include "unitgenerator.h"

using namespace CodeGen;

UnitGenerator::UnitGenerator(const AST::Unit* unit, const QDir& outputDir)
    : m_outputDir(outputDir), m_unit(unit)
{
}

QString UnitGenerator::generate()
{
    m_imports.clear();

    m_unit->accept(this);

    return QString();
}

void UnitGenerator::visit(const AST::ImportDeclaration* import)
{
    if (!m_gatherImports) {
        return;
    }

    m_imports << import->name();
}

void UnitGenerator::visit(const AST::Package* package)
{
    if (m_gatherImports)
        return;

    PackageGenerator pg(package);
    pg.generate();
}

void UnitGenerator::visit(const AST::TypeDeclaration* type)
{
    if (m_gatherImports)
        return;

    QFile file(QFileInfo(m_outputDir, type->name() + ".java").filePath());
    file.open(QIODevice::ReadWrite | QIODevice::Truncate);

    QTextStream out(&file);

    foreach (const QString& import, m_imports) {
        out << "import " << import << ";\n";
    }

    if (!m_imports.isEmpty()) {
        out << "\n";
    }

    foreach (const AST::ImportDeclaration* import, type->imports()) {
        out << "import " << import->name() << ";\n";
    }

    if (!type->imports().isEmpty()) {
        out << "\n";
    }

    TypeGenerator tg(type);
    tg.m_generateStatic = false;
    out << tg.generate();
}

void UnitGenerator::visit(const AST::Unit* unit)
{
    m_gatherImports = true;

    // look for global imports first
    for (int i = 0; i < unit->length(); ++i) {
        unit->at(i)->accept(this);
    }

    m_gatherImports = false;

    for (int i = 0; i < unit->length(); ++i) {
        unit->at(i)->accept(this);
    }
}
