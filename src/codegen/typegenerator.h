/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef CODEGEN_TYPEGENERATOR_H
#define CODEGEN_TYPEGENERATOR_H

#include <ast/typedeclaration.h>
#include <codegen/abstractgenerator.h>

namespace CodeGen {

class TypeGenerator : public CodeGen::AbstractGenerator
{
public:
    TypeGenerator(const AST::TypeDeclaration* type);

    virtual QString generate();
    virtual void visit(const AST::TypeDeclaration* typeDeclaration);
    virtual void visit(const AST::Method* method);
    virtual void visit(const AST::Field* field);

protected:
    QString m_string;

    const AST::TypeDeclaration *m_type;

    bool m_generateStatic;

    friend class UnitGenerator;
    friend class PackageGenerator;
};

}

#endif // CODEGEN_TYPEGENERATOR_H
