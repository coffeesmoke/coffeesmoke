/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <QTextStream>

#include <ast/field.h>
#include <ast/method.h>

#include "methodgenerator.h"
#include "fieldgenerator.h"
#include "typegenerator.h"

using namespace CodeGen;

TypeGenerator::TypeGenerator(const AST::TypeDeclaration* type)
    : m_type(type), m_generateStatic(true)
{
}

QString TypeGenerator::generate()
{
    m_string.clear();

    m_type->accept(this);

    return m_string;
}

void TypeGenerator::visit(const AST::TypeDeclaration* typeDeclaration)
{
    QTextStream out(&m_string);

    out << indentString();

    switch (typeDeclaration->access()) {
        case AST::Member::Private:
            out << "private ";
            break;

        case AST::Member::Protected:
            out << "protected ";
            break;

        case AST::Member::Public:
            out << "public ";
            break;
    }

    if ((typeDeclaration->modifiers() & AST::Member::Static) && m_generateStatic) {
        out << "static ";
    }
    if (typeDeclaration->modifiers() & AST::Member::Final) {
        out << "final ";
    }
    if (typeDeclaration->modifiers() & AST::Member::Abstract) {
        out << "abstract ";
    }

    out << (typeDeclaration->isInterface() ? "interface " : "class ") << typeDeclaration->name();

    if (!typeDeclaration->baseTypes().isEmpty()) {
        out << " extends " << typeDeclaration->baseTypes().join(", ");
    }

    if (!typeDeclaration->interfaces().isEmpty()) {
        out << " implements " << typeDeclaration->interfaces().join(", ");
    }

    out << " {\n\n";

    setIndent(indent() + 4);
    foreach (const AST::Member* member, typeDeclaration->members()) {
        // visit() methods will take care of generating the members
        member->accept(this);
        out << "\n";
    }
    setIndent(indent() - 4);

    out << indentString() << "}\n";
}

void TypeGenerator::visit(const AST::Method* method)
{
    QTextStream out(&m_string);

    MethodGenerator mg(method);

    mg.setGenerateBodies(!m_type->isInterface());
    mg.setIndent(indent());

    out << mg.generate();
}

void TypeGenerator::visit(const AST::Field* field)
{
    QTextStream out(&m_string);

    FieldGenerator fg(field);

    fg.setGenerateInitializer(!m_type->isInterface());
    fg.setIndent(indent());

    out << fg.generate();
}
