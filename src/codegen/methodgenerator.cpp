/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <QTextStream>

#include "methodgenerator.h"
#include "statementgenerator.h"

using namespace CodeGen;

MethodGenerator::MethodGenerator(const AST::Method* method)
    : m_method(method)
{
}

QString MethodGenerator::generate()
{
    m_string.clear();

    m_method->accept(this);

    return m_string;
}

bool MethodGenerator::generateBodies() const
{
    return m_generateBodies;
}

void MethodGenerator::setGenerateBodies(bool generate)
{
    m_generateBodies = generate;
}

void MethodGenerator::visit(const AST::Method* method)
{
    QTextStream out(&m_string);

    out << indentString();

    switch (method->access()) {
        case AST::Member::Private:
            out << "private ";
            break;

        case AST::Member::Protected:
            out << "protected ";
            break;

        case AST::Member::Public:
            out << "public ";
            break;
    }

    if (method->modifiers() & AST::Declaration::Static) {
        out << "static ";
    } else if (method->modifiers() & AST::Declaration::Final) {
        out << "final ";
    } else if (method->modifiers() & AST::Declaration::Abstract) {
        out << "abstract ";
    }

    out << method->type() << ' '  << method->name() << "( ";

    foreach (const AST::Parameter* param, method->parameters()) {
        param->accept(this);
    }

    out << m_parameters.join(", ");

    out << " )";

    if (!m_generateBodies) {
        out << ";\n";
        return;
    }

    out << " {\n";

    foreach (const AST::Statement* statement, method->statements()) {
        StatementGenerator statementGen(statement);
        statementGen.setIndent(indent() + 4);
        out << statementGen.generate();
    }

    out << indentString() << "}\n";
}

void MethodGenerator::visit(const AST::Parameter* param)
{
    QString paramString = param->type();
    paramString.append(' ');
    paramString.append(param->name());

    m_parameters << paramString;
}
