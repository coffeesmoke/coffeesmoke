/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <QTextStream>

#include <ast/assignmentexpression.h>
#include <ast/codesnippetexpression.h>
#include <ast/initializerlistexpression.h>
#include <ast/methodcallexpression.h>
#include <ast/newexpression.h>
#include <ast/primitiveexpression.h>
#include <ast/referenceexpression.h>

#include "expressiongenerator.h"

using namespace CodeGen;

ExpressionGenerator::ExpressionGenerator(AST::Expression* expression)
    : m_expression(expression)
{
}

QString ExpressionGenerator::generate()
{
    m_string.clear();

    m_expression->accept(this);

    return m_string;
}

void ExpressionGenerator::visit(const AST::AssignmentExpression* assign)
{
    QTextStream out(&m_string);

    assign->lhs()->accept(this);

    out << " = ";

    assign->rhs()->accept(this);
}

void ExpressionGenerator::visit(const AST::CodeSnippetExpression* snippet)
{
    QTextStream out(&m_string);

    out << snippet->snippet();
}

void ExpressionGenerator::visit(const AST::MethodCallExpression* methodCallExpression)
{
    QTextStream out(&m_string);

    if (methodCallExpression->target()->isValid()) {
        methodCallExpression->target()->accept(this);
        out << '.';
    }

    out << methodCallExpression->methodName() << "( ";
    for (int i = 0; i < methodCallExpression->parameters().length(); ++i) {
        if (i > 0) {
            out << ", ";
        }

        const AST::Expression* expr = methodCallExpression->parameters().at(i);
        expr->accept(this);
    }
    out << " )";
}

void ExpressionGenerator::visit(const AST::InitializerListExpression* initList)
{
    QTextStream out(&m_string);

    out << "{ ";
    for (int i = 0; i < initList->expressions().length(); ++i) {
        if (i > 0) {
            out << ", ";
        }

        const AST::Expression* expr = initList->expressions().at(i);
        expr->accept(this);
    }
    out << " }";
}

void ExpressionGenerator::visit(const AST::NewExpression* newExpression)
{
    QTextStream out(&m_string);

    out << "new " << newExpression->type() << "( ";

    for (int i = 0; i < newExpression->parameters().length(); ++i) {
        if (i > 0) {
            out << ", ";
        }

        const AST::Expression* expr = newExpression->parameters().at(i);
        expr->accept(this);
    }

    out << " )";
}

void ExpressionGenerator::visit(const AST::PrimitiveExpression* primitive)
{
    QTextStream out(&m_string);

    if (primitive->value().type() == QVariant::String) {
        out << '\"' << primitive->value().toString() << '\"';
    } else {
        // TODO: might need to be improved to output '42.0' for float types (instead of '42')
        out << primitive->value().toString();
    }
}

void ExpressionGenerator::visit(const AST::ReferenceExpression* reference)
{
    QTextStream out(&m_string);

    out << reference->target();
}
