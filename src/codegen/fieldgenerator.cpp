/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <QTextStream>

#include "expressiongenerator.h"
#include "fieldgenerator.h"

using namespace CodeGen;

FieldGenerator::FieldGenerator(const AST::Field* field)
    : m_field(field)
{
}

bool FieldGenerator::generateInitializer() const
{
    return m_generateInitializer;
}

void FieldGenerator::setGenerateInitializer(bool generate)
{
    m_generateInitializer = generate;
}

QString FieldGenerator::generate()
{
    m_string.clear();

    m_field->accept(this);

    return m_string;
}

void FieldGenerator::visit(const AST::Field* field)
{
    QTextStream out;

    out << indentString();

    switch (field->access()) {
        case AST::Member::Private:
            out << "private ";
            break;

        case AST::Member::Protected:
            out << "protected ";
            break;

        case AST::Member::Public:
            out << "public ";
            break;
    }

    if (field->modifiers() & AST::Member::Static) {
        out << "static ";
    }
    if (field->modifiers() & AST::Member::Final) {
        out << "final ";
    }
    if (field->modifiers() & AST::Member::Abstract) {
        out << "abstract ";
    }

    out << field->type() << " " << field->name();

    if (field->initExpression()->isValid() && m_generateInitializer) {
        ExpressionGenerator eg(field->initExpression());
        out << " = " << eg.generate();
    }

    out << ";\n";
}

