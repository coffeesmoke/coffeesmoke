/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef CODEGEN_EXPRESSIONGENERATOR_H
#define CODEGEN_EXPRESSIONGENERATOR_H

#include <ast/expression.h>
#include <codegen/abstractgenerator.h>

namespace CodeGen {

class ExpressionGenerator : public CodeGen::AbstractGenerator
{
public:
    ExpressionGenerator(AST::Expression *expression);

    virtual QString generate();
    virtual void visit(const AST::AssignmentExpression* assign);
    virtual void visit(const AST::CodeSnippetExpression* snippet);
    virtual void visit(const AST::MethodCallExpression* methodCallExpression);
    virtual void visit(const AST::NewExpression* newExpression);
    virtual void visit(const AST::InitializerListExpression* initList);
    virtual void visit(const AST::PrimitiveExpression* primitive);
    virtual void visit(const AST::ReferenceExpression* reference);

protected:
    AST::Expression *m_expression;
    QString m_string;
};

}

#endif // CODEGEN_EXPRESSIONGENERATOR_H
