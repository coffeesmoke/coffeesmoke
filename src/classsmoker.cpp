/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "classsmoker.h"
#include "util.h"

ClassSmoker::ClassSmoker(AST::Package* package, Smoke* smoke)
    : AbstractSmoker(smoke), m_package(package)
{
}

ClassSmoker::~ClassSmoker()
{
}

void ClassSmoker::run()
{
    for (Smoke::Index i = 1; i <= m_smoke->numClasses; ++i) {
        Smoke::Class *klass = m_smoke->classes + i;
        if (klass->external) {
            continue;
        }

        defineClass(klass->className);
    }
}

AST::TypeDeclaration* ClassSmoker::defineClass(const QString& name)
{
    if (stringTypeMap.contains(name)) {
        return stringTypeMap[name];
    }

    QString className = Util::classNameOnly(name);
    QString container = Util::container(name);

    AST::TypeDeclaration* typeDecl = new AST::TypeDeclaration(className);

    if (!container.isEmpty()) {
        defineClass(container)->members().append(typeDecl);
    } else {
        m_package->append(typeDecl);
    }

    stringTypeMap[name] = typeDecl;

    return typeDecl;
}
