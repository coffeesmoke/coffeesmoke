/*
    This file is part of coffeesmoke.
    Copyright 2010, Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef UTIL_H
#define UTIL_H

#include <QString>

class Util
{
public:
    /**
     * Strips the last '::Foo' part of \c className.
     */
    static inline QString container(const QString& className) {
        int idx = className.lastIndexOf("::");
        if (idx < 0) {
            return QString();
        }
        return className.left(idx);
    }

    /**
     * Returns only the part after the last '::' of \c name.
     */
    static inline QString classNameOnly(const QString& name) {
        int idx = name.lastIndexOf("::");
        if (idx < 0) {
            return name;
        }
        return name.mid(idx + 2);
    }
};

#endif // UTIL_H
